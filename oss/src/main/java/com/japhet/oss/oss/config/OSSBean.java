package com.japhet.oss.oss.config;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;

/**
 * oss 配置
 */
public class OSSBean {

    @Bean
    @Scope("prototype")
    public static OSS ossClient() {
        String endpoint = "oss-cn-xxx.xx.com";//你的endpoint
        String accessKeyId = "xxxx";//你的accessKeyId
        String accessKeySecret = "xxxxxx";//你的accessKeySecret
        return new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);
    }
}
