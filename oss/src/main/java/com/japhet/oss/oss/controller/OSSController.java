package com.japhet.oss.oss.controller;

import com.japhet.oss.oss.common.oss.AliOSSUtil;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * OSS文件上传
 */
@RestController
@RequestMapping("/japhet/oss")
public class OSSController  {

    @RequestMapping("/uploadImg")
    public String uploadImg(@RequestParam(value = "file", required = false) MultipartFile file){
        return AliOSSUtil.uploadImg(file, 5);
    }
}
